
local S = ethereal.intllib


-- Wood drawers

drawers.register_drawer("ethereal:sakura_wood_drawer", {
	description = S("Sakura Wood"),
	tiles1 = drawers.node_tiles_front_other("drawers_sakura_wood_front_1.png",
		"drawers_sakura_wood.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_sakura_wood_front_2.png",
		"drawers_sakura_wood.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_sakura_wood_front_4.png",
		"drawers_sakura_wood.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:sakura_wood"
})
	
drawers.register_drawer("ethereal:yellow_wood_drawer", {
	description = S("Yellow Wood"),
	tiles1 = drawers.node_tiles_front_other("drawers_yellow_wood_front_1.png",
		"drawers_yellow_wood.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_yellow_wood_front_2.png",
		"drawers_yellow_wood.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_yellow_wood_front_4.png",
		"drawers_yellow_wood.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:yellow_wood"
})
	
drawers.register_drawer("ethereal:willow_wood_drawer", {
	description = S("Willow Wood"),
	tiles1 = drawers.node_tiles_front_other("drawers_willow_wood_front_1.png",
		"drawers_willow_wood.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_willow_wood_front_2.png",
		"drawers_willow_wood.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_willow_wood_front_4.png",
		"drawers_willow_wood.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:willow_wood"
})

drawers.register_drawer("ethereal:redwood_wood_drawer", {
	description = S("Redwood Wood"),
	tiles1 = drawers.node_tiles_front_other("drawers_redwood_wood_front_1.png",
		"drawers_redwood_wood.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_redwood_wood_front_2.png",
		"drawers_redwood_wood.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_redwood_wood_front_4.png",
		"drawers_redwood_wood.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:redwood_wood"
})

drawers.register_drawer("ethereal:palm_wood_drawer", {
	description = S("Palm Wood"),
	tiles1 = drawers.node_tiles_front_other("drawers_palm_wood_front_1.png",
		"drawers_palm_wood.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_palm_wood_front_2.png",
		"drawers_palm_wood.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_palm_wood_front_4.png",
		"drawers_palm_wood.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:palm_wood"
})

drawers.register_drawer("ethereal:olive_wood_drawer", {
	description = S("Olive Wood"),
	tiles1 = drawers.node_tiles_front_other("drawers_olive_wood_front_1.png",
		"drawers_olive_wood.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_olive_wood_front_2.png",
		"drawers_olive_wood.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_olive_wood_front_4.png",
		"drawers_olive_wood.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:olive_wood"
})

drawers.register_drawer("ethereal:frost_wood_drawer", {
	description = S("Frost Wood"),
	tiles1 = drawers.node_tiles_front_other("drawers_frost_wood_front_1.png",
		"drawers_frost_wood.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_frost_wood_front_2.png",
		"drawers_frost_wood.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_frost_wood_front_4.png",
		"drawers_frost_wood.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:frost_wood"
})

drawers.register_drawer("ethereal:birch_wood_drawer", {
	description = S("Birch Wood"),
	tiles1 = drawers.node_tiles_front_other("drawers_birch_wood_front_1.png",
		"drawers_birch_wood.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_birch_wood_front_2.png",
		"drawers_birch_wood.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_birch_wood_front_4.png",
		"drawers_birch_wood.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:birch_wood"
})

drawers.register_drawer("ethereal:banana_wood_drawer", {
	description = S("Banana Wood"),
	tiles1 = drawers.node_tiles_front_other("drawers_banana_wood_front_1.png",
		"drawers_banana_wood.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_banana_wood_front_2.png",
		"drawers_banana_wood.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_banana_wood_front_4.png",
		"drawers_banana_wood.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:banana_wood"
})

drawers.register_drawer("ethereal:bamboo_wood_drawer", {
	description = S("Bamboo Wood"),
	tiles1 = drawers.node_tiles_front_other("drawers_bamboo_wood_front_1.png",
		"drawers_bamboo_wood.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_bamboo_wood_front_2.png",
		"drawers_bamboo_wood.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_bamboo_wood_front_4.png",
		"drawers_bamboo_wood.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:bamboo_block"
})

drawers.register_drawer("ethereal:mushroom_drawer", {
	description = S("Mushroom"),
	tiles1 = drawers.node_tiles_front_other("drawers_mushroom_front_1.png",
		"drawers_mushroom.png"),
	tiles2 = drawers.node_tiles_front_other("drawers_mushroom_front_2.png",
		"drawers_mushroom.png"),
	tiles4 = drawers.node_tiles_front_other("drawers_mushroom_front_4.png",
		"drawers_mushroom.png"),
	groups = {choppy = 3, oddly_breakable_by_hand = 2},
	sounds = drawers.WOOD_SOUNDS,
	drawer_stack_max_factor = 4 * 8, 
	material = "ethereal:mushroom_trunk"
})


-- Drawer upgrades
	
drawers.register_drawer_upgrade("ethereal:drawer_upgrade_etherium", {
	description = S("Etherium Drawer Upgrade (x22)"),
	inventory_image = "drawers_upgrade_etherium.png",
	groups = {drawer_upgrade = 2100},
	recipe_item = "ethereal:etherium_dust"
})
