--[[

 Fishing routine partially inspired by Rootyjr's fishing mod for MineClone2

]]--

local S = ethereal.intllib

local fish_items = {
	"ethereal:tuna_raw",
	"ethereal:herring_raw",
	{"mobs_fish:clownfish", "savanna"},
	{"ethereal:fish_cichlid", "jungle"},
	{"mobs_jellyfish:jellyfish", "ocean"},
	{"mobs_fish:tropical", "jungle"},
	{"ethereal:fish_trout", "ocean"},
	{"ethereal:fish_salmon", "ocean"},
}

local river_fishes = {
	"ethereal:fish_salmon",
	"ethereal:fish_trout",
	"ethereal:fish_cichlid"
}

local junk_items = {
	"default:stick",
	"farming:string",
	"dye:black",
	{"ethereal:bamboo", "bamboo"}
}

local bonus_items = {
	"mobs:nametag",
	"ethereal:empty_can",
	{"ethereal:crystal_spike", "frost"},
	{"ethereal:banana_bunch", "grove"}
}

local default_item = "default:dirt"

local random = math.random -- yup we use this a lot

local rod_use = function(itemstack, placer, pointed_thing)

	local inv = placer:get_inventory()

	if inv:contains_item("main", "ethereal:worm") then

		inv:remove_item("main", "ethereal:worm")

		return ItemStack("ethereal:fishing_rod_baited")
	end
end

--add item function (for API)
ethereal.add_item = function(fish, river_fish, junk, bonus)

	if fish and fish ~= "" then
		table.insert(fish_items, fish)
	end
	
	if river_fish and river_fish ~= "" then
		table.insert(river_fishes, river_fish)
	end

	if junk and junk ~= "" then
		table.insert(junk_items, junk)
	end

	if bonus and bonus ~= "" then
		table.insert(bonus_items, bonus)
	end
end

-- narrow item list depending on biome if applicable
local find_item = function(list, pos)

	local item
	local items = {}
	local data= minetest.get_biome_data(pos)
	local biome = data and minetest.get_biome_name(data.biome) or ""

	for n = 1, #list do

		item = list[n]

		if type(item) == "string" then

			table.insert(items, item)

		elseif type(item) == "table" then

			if item[2] == "" or biome:find(item[2]) then
				table.insert(items, item[1])
			end
		end

	end

--print("==biome: " .. biome, dump(items))

	if #items > 0 then
		return items[random(#items)]
	end

	return ""
end


local fish = {
	{"Salmon", "salmon", 2},
	{"Cichlid", "cichlid", 2},
	{"Trout", "trout", 2},
	{"Tuna", "tuna", 2},
	{"Herring", "herring", 2}
}

for n = 1, #fish do

	local usage
	local groups

	if fish[n][3] > 0 then
		usage = minetest.item_eat(fish[n][3])
		groups = {food_fish_raw = 1, ethereal_fish = 1}
	end

	minetest.register_craftitem("ethereal:fish_" .. fish[n][2], {
		description = S(fish[n][1]),
		inventory_image = "ethereal_fish_" .. fish[n][2] .. ".png",
		on_use = usage,
		groups = groups
	})
end

local fishing_action = function (itemstack, user, pointed_thing)

	if pointed_thing.type ~= "node" then
		return
	end

	local pos = pointed_thing.under
	local node = minetest.get_node(pos).name

	if (node == "default:water_source"
	or node == "default:river_water_source")
	and random(1, 100) < 5 then

		--local type = ethereal.fish[random(1, #ethereal.fish)][1]
		local item
		local r = random(100)

		if r < 86 then
			
			if node == "default:water_source" then
			
				item = find_item(fish_items, pos)
				
			elseif node == "default:river_water_source" then
			
				item = find_item(river_fishes, pos)
				
			end

		elseif r > 85 and r < 96 then

			item = find_item(junk_items, pos)

		else

			item = find_item(bonus_items, pos)
		end

		-- make sure item exists, if not replace with default item
		if not minetest.registered_items[item] then
			item = default_item
		end

--print ("---caught", item, r)

		local inv = user:get_inventory()

		if inv:room_for_item("main", {name = item}) then

			inv:add_item("main", {name = item})

			minetest.sound_play("default_water_footstep", {pos = pos})

			pos.y = pos.y + 0.5

			minetest.add_particlespawner({
				amount = 5,
				time = .3,
				minpos = pos,
				maxpos = pos,
				minvel = {x = 2, y = .5, z = 2},
				maxvel = {x = 2, y = .5, z = 2},
				minacc = {x = 1, y = .1, z = 1},
				maxacc = {x = 1, y = .1, z = 1},
				minexptime = .3,
				maxexptime = .5,
				minsize = .5,
				maxsize = 1,
				collisiondetection = false,
				vertical = false,
				texture = "bubble.png",
				playername = "singleplayer"
			})

			return ItemStack("ethereal:fishing_rod")
		else
			minetest.chat_send_player(user:get_player_name(),
				S("Inventory full, Fish Got Away!"))
		end
	end
end

-- Fishing Rod
minetest.register_craftitem("ethereal:fishing_rod", {
	description = S("Fishing Rod (Right-Click with rod to bait with worm from inventory)"),
	inventory_image = "fishing_rod.png",
	wield_image = "fishing_rod.png",

	on_place = rod_use,
	on_secondary_use = rod_use
})

minetest.register_craft({
	output = "ethereal:fishing_rod",
	recipe = {
			{"","","group:stick"},
			{"", "group:stick", "farming:string"},
			{"group:stick", "", "farming:string"},
		}
})

minetest.register_craft({
	type = "fuel",
	recipe = "ethereal:fishing_rod",
	burntime = 15,
})

-- Fishing Rod (Baited)
minetest.register_craftitem("ethereal:fishing_rod_baited", {
	description = S("Baited Fishing Rod"),
	inventory_image = "fishing_rod_baited.png",
	wield_image = "fishing_rod_wield.png",
	stack_max = 1,
	liquids_pointable = true,

	on_use = fishing_action
})

minetest.register_craft({
	type = "shapeless",
	output = "ethereal:fishing_rod_baited",
	recipe = {"ethereal:fishing_rod", "ethereal:worm"},
})

-- Sift through 2 Dirt Blocks to find Worm
minetest.register_craft({
	output = "ethereal:worm",
	recipe = {
		{"default:dirt","default:dirt"},
	}
})

-- cooked fish
minetest.register_craftitem(":ethereal:fish_cooked", {
	description = S("Cooked Fish"),
	inventory_image = "ethereal_fish_cooked.png",
	wield_image = "ethereal_fish_cooked.png",
	groups = {food_fish = 1, flammable = 3},
	on_use = minetest.item_eat(5),
})

minetest.register_craft({
	type = "cooking",
	output = "ethereal:fish_cooked",
	recipe = "group:ethereal_fish",
	cooktime = 8,
})

-- Sashimi (Thanks to Natalia Grosner for letting me use the sashimi image)
minetest.register_craftitem("ethereal:sashimi", {
	description = S("Sashimi"),
	inventory_image = "ethereal_sashimi.png",
	wield_image = "ethereal_sashimi.png",
	on_use = minetest.item_eat(4)
})

minetest.register_craft({
	output = "ethereal:sashimi 2",
	recipe = {
		{"group:food_seaweed", "group:food_fish_raw", "group:food_seaweed"},
	}
})

-- Worm
minetest.register_craftitem("ethereal:worm", {
	description = S("Worm"),
	inventory_image = "ethereal_worm.png",
	wield_image = "ethereal_worm.png"
})

minetest.register_craft({
	output = "ethereal:worm",
	recipe = {
		{"default:dirt","default:dirt"}
	}
})

-- Cooked tuna
minetest.register_craftitem("ethereal:tuna_cooked", {
	description = S("Cooked Tuna"),
	inventory_image = "tuna_cooked.png",
	wield_image = "tuna_cooked.png",
	groups = {food_tuna = 1, flammable = 3},
	on_use = minetest.item_eat(3),
})

minetest.register_craft({
	type = "cooking",
	output = "ethereal:tuna_cooked",
	recipe = "ethereal:tuna_raw",
	cooktime = 3,
})

-- Canned tuna
register_canned_food("ethereal:tuna_raw", nil, 6)

minetest.override_item("ethereal:tuna_canned", {
    groups = {food_tuna_canned = 1, canned_fish = 1, flammable = 2}  -- prevent breaking farming:ensaladilla_rusa recipe
})

minetest.register_alias("ethereal:tuna_canned_empty", "ethereal:empty_can")

-- Canned herring
register_canned_food("ethereal:herring_raw", nil, 6)

-- Surströmming
minetest.register_craftitem("ethereal:surstromming_canned", {
	description = S("Canned Surströmming"),
	inventory_image = "surstroemming.png",
	wield_image = "surstroemming.png",
	groups = {canned_fish = 1, flammable = 2},
	on_use = function(itemstack, user, pointed_thing)
		if user then
			if math.random(5) < 3 then
				return minetest.do_item_eat(-5, "ethereal:empty_can", itemstack, user, pointed_thing)
			else
				return minetest.do_item_eat(6, "ethereal:empty_can", itemstack, user, pointed_thing)
			end
		end
	end
})

minetest.register_craftitem("ethereal:surstromming_fillet", {
	description = S("Surströmming Fillet"),
	inventory_image = "surstroemming_fillet.png",
	wield_image = "surstroemming_fillet.png",
	on_use = minetest.item_eat(6),
	groups = {food_surstromming = 1},
})

minetest.register_craft({
	type = "shapeless",
	output = "ethereal:surstromming_fillet",
	recipe = {"ethereal:surstromming_canned"},
	replacements = {
		{"ethereal:surstromming_canned", "ethereal:empty_can"}
	}
})

-- compatibility
minetest.register_alias("ethereal:fish_raw", "ethereal:fish_cichlid")
minetest.register_alias("ethereal:tuna_raw", "ethereal:fish_tuna")
minetest.register_alias("ethereal:herring_raw", "ethereal:fish_herring")
